FROM php:8.2-apache

RUN docker-php-ext-install mysqli && a2enmod rewrite

RUN apt-get -y update
RUN apt-get -y install git
RUN apt-get -y install zip

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

CMD bash -c "composer install && ./vendor/bin/phpunit tests --configuration phpunit-docker.xml"
