<?php
	namespace Jca\State;

	/**
	 * State interface
	 * @author Julien Caillabet <jcaillabet@gmail.com>
	 * @access public
	 */
	class ConcreteState implements State
	{
		protected Context $context;

		public function __construct(Context $context)
		{
			$this->context = $context;
		}

		public function getContext(): Context
		{
			return $this->context;
		}
	}
?>
