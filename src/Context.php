<?php
	namespace Jca\State;

	/**
	 * Context class
	 * @author Julien Caillabet <jcaillabet@gmail.com>
	 * @access public
	 */
	class Context
	{
		/**
		 * Context constructor
		 * @method __construct
		 * @param  State  $state  first state of context
		 */
		public function __construct(protected ?State $state) {}

		/**
		 * Change current state
		 * @method setState
		 * @param  State  $state  new state to set
		 */
		public function setState($state)
		{
			$this->state = $state;
		}
	}
?>
