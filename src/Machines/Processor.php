<?php
	namespace Jca\State\Machines;

	use Jca\Engine\Exceptions\HTTPException;
	use Jca\State\Context;
	use Jca\State\State;
	use Jca\State\Machines\Processor\ProcessorState;

	/**
	 * Description of a state machine that create an output from an input
	 */
	abstract class Processor extends Context
	{
		protected array $stack;

		/**
		 * Summary of __construct
		 * @param \Jca\State\State $state
		 * @param array $input
		 * @param bool $auto
		 */
		public function __construct(State $state, $input)
		{
			parent::__construct($state);

			$this->stack = $this->init($input);

			if(!$this->stack)
				throw new HTTPException(500);
		}

		abstract protected function init($input) : array;

		/**
		 * Get the stack
		 * @method getStack
		 * @return object  Processor stack
		 */
		public function getStack():array{ return $this->stack; }

		/**
		 * Get the actual state
		 *
		 * @return State
		 */
		public function getState()
		{
			return $this->state;
		}

		/**
		 * Change current state
		 * @method setState
		 * @param  State  $state  new state to set
		 */
		public function setState($state)
		{
			$this->state = $state;
		}

		/**
		 * Get the next part
		 * @return mixed next part
		 */
		public function unstack()
        {
			return array_shift($this->stack);
        }

		/**
		 * Execution of current state
		 * @method request
		 */
		public function process()
		{
			if(empty($this->getState()))
				throw new HTTPException(500);

			do
			{
				$this->getState()->process();
			} while ($this->getState());
		}
	}
?>
