<?php
	namespace Jca\State;

	/**
	 * State interface
	 * @author Julien Caillabet <jcaillabet@gmail.com>
	 * @access public
	 */
	interface State
	{
		public function getContext() : Context;	
	}
?>
