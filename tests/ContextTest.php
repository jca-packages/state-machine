<?php
	declare(strict_types=1);

	require __DIR__ . '/../vendor/autoload.php';

	use PHPUnit\Framework\TestCase;
	use function PHPUnit\Framework\assertEquals;
	use function PHPUnit\Framework\assertNotNull;
	use function PHPUnit\Framework\assertTrue;

	use Jca\State\Tests\TestProcessor;
	
	spl_autoload_register(function ($class_name) 
	{
		$folders = ['/src/', '/../src/'];
		foreach($folders as $folder)
		{
			$file = __DIR__ . $folder . $class_name . '.php';
			if(file_exists($file))
				$file = __DIR__ . $folder . $class_name . '.php';
		}
	});

	/**
	 * State machine tests class
	 */
	final class ContextTest extends TestCase
	{
		function testStack(){
			$input = "Test string";
			
			$processor = new TestProcessor($input);
			assertNotNull($processor->getStack());
		}
	}
?>
