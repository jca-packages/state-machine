<?php
	namespace Jca\State\Tests;

	use	Jca\State\ConcreteState;
	
	require __DIR__ . '/../../vendor/autoload.php';

    /**
	 * State machine tests class
	 */
	final class TestProcessorState extends ConcreteState
	{
		public function process()
		{
			$word = $this->context->unstack();

			if(!empty($word))
				$this->context->setState(new TestProcessorState($this->context));
			else
				$this->context->setState(null);
		}
	}
?>
