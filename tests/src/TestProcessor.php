<?php
	namespace Jca\State\Tests;

    use Jca\State\Machines\Processor;
	
    /**
	 * State machine tests class
	 */
	final class TestProcessor extends Processor
	{
		public function __construct(string $input)
		{
			parent::__construct(new TestProcessorState($this), $input);
		}

		protected function init($input): array
		{
			return str_split($input);
		}
	}
?>
